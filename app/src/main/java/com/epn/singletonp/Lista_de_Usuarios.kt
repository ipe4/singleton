package com.epn.singletonp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class Lista_de_Usuarios : AppCompatActivity() {
    lateinit var mUsers_RecyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_de__usuarios)
        mUsers_RecyclerView = findViewById(R.id.user_recyclerview)
        mUsers_RecyclerView.layoutManager = LinearLayoutManager(this)


        mUsers_RecyclerView.adapter = userAdapter()
    }
}
