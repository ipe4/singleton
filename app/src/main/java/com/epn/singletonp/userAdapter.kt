package com.epn.singletonp


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class userAdapter : RecyclerView.Adapter<userHolder>() {

    object singleton {
        val UsersList: MutableList<User> = mutableListOf<User>()


        init {
            for (i in 1..100) {
                val usuario = User().apply {
                    Name = "Usuario" + i
                    email = "email" + i
                }
                UsersList.add(usuario)
            }


        }
    }

    override fun getItemCount(): Int {
        return singleton.UsersList.size
    }

    override fun onBindViewHolder(holder: userHolder, position: Int) {

        holder.Name.text = singleton.UsersList[position].Name
        holder.email.text = singleton.UsersList[position].email
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): userHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.user_holder, parent, false)
        return userHolder(view)
    }


}